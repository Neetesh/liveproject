package first.xyzt.com.liveproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class TermsActivity extends AppCompatActivity implements View.OnClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.terms_activity);

        ImageView img = findViewById(R.id.img_leftarrow);
        img.setOnClickListener(this);

        TextView textView = findViewById(R.id.tv_confirm);
        textView.setOnClickListener(this);



    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_leftarrow:
                finish();
                break;

            case R.id.tv_confirm:
                Intent in = new Intent(this,MainActivity.class);
                startActivity(in);
                break;


        }
    }
}

package first.xyzt.com.liveproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class VerificationActivity extends AppCompatActivity implements View.OnClickListener {

    public ImageView imageView4;
    public ImageView imageView3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.verification_activity);
        imageView4 = findViewById(R.id.Image_imageview2);
        imageView4.setOnClickListener(this);
        imageView3 = findViewById(R.id.img_backarrow3);
        imageView3.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.Image_imageview2:
                Intent intent = new Intent(VerificationActivity.this, EmailVerifyActivity.class);
                startActivity(intent);
                break;
            case R.id.img_backarrow3:
                finish();
                break;

        }
    }
}

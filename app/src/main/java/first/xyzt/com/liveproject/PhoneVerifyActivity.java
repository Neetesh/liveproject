package first.xyzt.com.liveproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class PhoneVerifyActivity extends AppCompatActivity implements View.OnClickListener {

    public ImageView imageView;
    public ImageView imageView1;
    public TextView textView;
    public LinearLayout linearLayout;
    private ImageView flagImage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.phone_verify_activity);


        imageView = findViewById(R.id.Right_view);
        imageView.setOnClickListener(this);

        imageView1 = findViewById(R.id.img_back);
        imageView1.setOnClickListener(this);

        linearLayout = findViewById(R.id.linear);
        linearLayout.setOnClickListener(this);

        flagImage = findViewById(R.id.img_countrycode);

        textView = findViewById(R.id.tv_textview);
        if (
                getIntent().getStringExtra("countryCode") != null) {
            int flag = getIntent().getIntExtra("flag", 0);
            String countrycode = getIntent().getStringExtra("countryCode");
            textView.setText(countrycode);
            flagImage.setImageResource(flag);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.Right_view:
                Intent intent = new Intent(PhoneVerifyActivity.this, VerificationActivity.class);
                startActivity(intent);
                break;

            case R.id.img_back:
                finish();
                break;
            case R.id.linear:
                Intent intent2 = new Intent(PhoneVerifyActivity.this, Countrycode.class);
                startActivity(intent2);
                break;
        }
    }
}





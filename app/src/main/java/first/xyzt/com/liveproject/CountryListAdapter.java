package first.xyzt.com.liveproject;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CountryListAdapter extends ArrayAdapter<String> {

    private final Activity context;
    private final String[] countryname;
    private final String[] countrynicname;
    private final int[] countrypic;
    private final String[] countrycode;

    public CountryListAdapter(Activity context, String[] countryname,String[] countrynicname, int[] countrypic, String[] countrycode) {
        super(context, R.layout.country, countryname);

        this.context = context;
        this.countryname = countryname;
        this.countrynicname = countrynicname;
        this.countrypic = countrypic;
        this.countrycode = countrycode;
    }
        public View getView(int position, View view, ViewGroup parent) {
            LayoutInflater inflater = context.getLayoutInflater();
            View rowView = inflater.inflate(R.layout.country, null, true);

            TextView titleText = (TextView) rowView.findViewById(R.id.tv_countryname);
            ImageView imageView = (ImageView) rowView.findViewById(R.id.img_countrypic);
            TextView subtitleText = (TextView) rowView.findViewById(R.id.tv_countrynicname);


            titleText.setText(countryname[position]);
            imageView.setImageResource(countrypic[position]);
            subtitleText.setText(countrynicname[position]);

            return rowView;
        };

}

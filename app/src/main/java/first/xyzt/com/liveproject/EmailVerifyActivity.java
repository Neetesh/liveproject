package first.xyzt.com.liveproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class EmailVerifyActivity extends AppCompatActivity implements View.OnClickListener {
    public ImageView imageView4;
    public ImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.email_verify_activity);

        imageView4 = findViewById(R.id.img_backarrow4);
        imageView4.setOnClickListener(this);

        img = findViewById(R.id.img_rightarrow);
        img.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_backarrow4:
                finish();
                break;

            case R.id.img_rightarrow:
                Intent i = new Intent(EmailVerifyActivity.this, TermsActivity.class);
                startActivity(i);
                break;
        }
    }
}

package first.xyzt.com.liveproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    public TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loginactivity);
        tv = findViewById(R.id.Sign_up);
        tv.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.Sign_up:
                Intent intent = new Intent(LoginActivity.this, PhoneVerifyActivity.class);
                startActivity(intent);
                break;
        }
    }
}

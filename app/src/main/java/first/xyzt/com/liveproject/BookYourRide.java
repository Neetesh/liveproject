package first.xyzt.com.liveproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;

public class BookYourRide extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_your_ride);
        CardView c = findViewById(R.id.cv_payment);
        c.setOnClickListener(this);

        ImageView img1 = findViewById(R.id.img_backarrow2);
        img1.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cv_payment:
                Intent i = new Intent(this, payment.class);
                startActivity(i);
                break;

            case R.id.img_backarrow2:
                finish();
                break;
        }
    }
}

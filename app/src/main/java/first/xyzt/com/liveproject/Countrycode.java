package first.xyzt.com.liveproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

public class Countrycode extends AppCompatActivity {
    public ListView countrylistview;

    CountryListAdapter adapter;

    String[] countryname = {"INDIA", "ARGENTINA", "AUSTRALIA", "BELGUIM", "BRAZIL", "CANADA", "CUBA", "FRANCE", "HONG KONG", "ITALY",};
    String[] countrynicname = {"RPS", "ARS", "AUD", "EURO", "REAL", "DOLLAR", "PESO", "EURO", "DOLLAR", "EURO",};
    String[] countrycode = {"+91", "+96", "+61", "+51", "+11", "+81", "+71", "+98", "+90", "+92"};
    int[] countrypic = {
            R.drawable.india,
            R.drawable.argentina,
            R.drawable.australia,
            R.drawable.belgium,
            R.drawable.canada,
            R.drawable.cuba,
            R.drawable.france,
            R.drawable.hongkong,
            R.drawable.italy,
            R.drawable.italy

    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_countrycode);



        adapter = new CountryListAdapter(Countrycode.this, countryname, countrynicname, countrypic, countrycode);
        countrylistview = findViewById(R.id.list_country);
        countrylistview.setAdapter(adapter);

        countrylistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(Countrycode.this, "you clicked on " + countryname[position], Toast.LENGTH_SHORT).show();
                Intent i = new Intent(Countrycode.this, PhoneVerifyActivity.class);

                i.putExtra("flag", countrypic[position]);
                i.putExtra("countryCode", countrycode[position]);
                startActivity(i);
                finish();

            }
        });
    }

}
